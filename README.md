# ICD Météo

## Description

ICD Meteo est une application web permettant de fournir des prévisions météorologiques “sonifiées”.
Elle est responsive et accessible aux personnes déficientes visuelles.

Techniquement, c'est une application écrite en Javascript, qui utilise le framework VueJS/Vuetify et la librairie Vite.

> Cette application est une maquette rapidement développée dans le cadre l'enseignement "Gestion de projets agiles et en équipe" par la promotion MS-ICD 2023.  

## Architecture de l'application

L'application "ICD Météo" est constituée de 2 parties:

Le frontend, chargé de l'interface avec l'utilisateur dans le navigateur.  
Les APIs externes:  

* Google Maps API: chargée de fournir la localisation  
* Openweathermap API: chargée de fournir les prévisions météo en fonction du lieu et de l'écheance.  

L'utilisateur final de l'application interagit avec le frontend.  
Le frontend est composé de fichiers statiques. Il execute du code javascript dans le navigateur de l'utilisateur, et effectue des requètes vers les APIs. Il permet à l'utilisateur d'avoir les prévisions météo avec une expérience sonore immersive.  

## Déploiement

En développement, l'application utilise le serveur "preview" intégré à Vite.  
En production, l'application utilise un serveur web externe (Nginx)  

Pour les différents modes opératoires, suivez les guides :

* [Installation en mode développement](./developpement.md)
* [Installation en mode production](./production.md)

## Clône de l'application

```shell
git clone https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/weather_app.git
```

## Déploiement avec conteneur

Pour déployer l'application, modifier les variables d'environnement dans le fichier **".env"** et faire :

```shell
cd weather_app
docker build -t weather .
docker run --rm --env-file=.env -p 80:8000 weather
```

Pour accéder à l'application, rendez-vous via un navigateur à l'adresse :

```shell
http://localhost:80
```


