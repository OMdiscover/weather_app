FROM nginx:perl
COPY env.sh .
COPY nginx.conf.template .
RUN chmod +x env.sh
RUN rm -rf /usr/share/nginx/html/*
COPY /dist /usr/share/nginx/html
ENTRYPOINT ["/env.sh"]
CMD ["nginx","-g","daemon off;"]