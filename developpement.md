# Déploiement en mode développement

## Description

Pour le déploiement en mode développement, le frontend est géré par le serveur "preview" intégré à NodeJS.  

Détails:

* Frontend :
  * utilise le serveur "preview"
  * envoie des requetes vers les APIs externes depuis le navigateur de l'utilisateur

## Préparation du système

* mise à jour du système

``` bash
sudo apt update
```

* Création de l'utilisateur "app-meteo"

``` bash
sudo useradd \
   -m -d /home/app-meteo \
   -r -c 'Meteo App system-user' \
   -s /bin/bash \
   app-meteo
```

* Installation des pré-requis

``` bash
sudo apt -y install git vim nano
```

## Initialisation de l'environnement

* Connexion en tant qu'utilisateur app-meteo

``` bash
sudo -i -u app-meteo
```

* Vérification

``` bash
id 
# uid=998(app-meteo) gid=998(app-meteo) groups=998(app-meteo)
```

* Installation de NVM (Node Version Manager)

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

* Vérification

```sh
command -v nvm
```

* Installation de Node JS

```sh
nvm install --lts
```

## Installation de l'application

* Récupération du dépot

```bash
cd $HOME
git clone https://gitlab.imt-atlantique.fr/rtb10361/projet_site_web_meteo_msicd.git icd-meteo
```

* Installation des dépendances Node JS  

```bash
cd $HOME/icd-meteo
npm install
```

* Compilation de l'application  

```sh
npm run build
```

Le répertoire __dist/__ est généré avec les fichiers statiques de l'application web  

## Déploiement de l'application

### Mise en place des pages

```sh
npm run preview
```

## Test de l'accès à l'application

Depuis un navigateur : <http://localhost:4173>  
