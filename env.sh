#!/bin/bash

export METEO_URL_SERVERNAME

export METEO_URL_PORT

export METEO_FRONTEND_ROOT

envsubst '${METEO_URL_SERVERNAME},${METEO_URL_PORT},${METEO_FRONTEND_ROOT}' < nginx.conf.template > nginx.conf

cp nginx.conf  /etc/nginx/conf.d/default.conf

exec "$@"


